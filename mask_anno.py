#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

import os
import cv2
import ntpath
import json
import numpy as np

class Labelme():
    def __init__(self, image_height, image_width, image_path, version="4.4.0"):
        self.version = version
        self.flag = {}
        self.shapes = []
        self.imagePath = ntpath.basename(image_path)
        self.imageData = None
        self.imageHeight = image_height
        self.imageWidth = image_width
        self.dir = os.path.dirname(image_path)

    def add_shape(self, shape_type, label, bound):
        shape = {
            "label": label,
            "shape_type": shape_type,
            "group_id": None,
            "flag": {}
        }
        if isinstance(bound, np.ndarray):
            bound = bound.tolist()
        if shape_type == "polygon":
            shape["points"] = bound
        else:
            raise NotImplementedError
        self.shapes.append(shape)

    def export(self, output_dir=None):
        if not output_dir:
            output_dir = self.dir
        export_file_name = self.imagePath.rsplit(".", 1)[0] + ".json"
        export_label_obj = {
            "version": self.version,
            "flag": self.flag,
            "shapes": self.shapes,
            "imagePath": self.imagePath,
            "imageData": self.imageData,
            "imageHeight": self.imageHeight,
            "imageWidth": self.imageWidth
        }
        with open(os.path.join(output_dir, export_file_name), 'w') as outfile:
            json.dump(export_label_obj, outfile)

def mask_to_edge(mask, min_length=10):
    ret, thresh = cv2.threshold(mask, 127, 255, 0)
    ret, thresh = cv2.threshold(mask, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    all_masks = []
    for i in range(len(contours)):
        cnt = contours[i].reshape(-1, 2)
        if cnt.shape[0] < min_length:
            continue
        all_masks.append(cnt)
    return all_masks

def read_img_gray(img_path):
    img = cv2.imread(img_path)
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return imgray


if __name__ == "__main__":
    ori_image_path = "/Users/willc/Downloads/WechatIMG6068.png"
    mask_img_path = "/Users/willc/Downloads/WechatIMG6068_mask.png"

    h, w, _ = cv2.imread(ori_image_path).shape
    mask_img = read_img_gray(mask_img_path)
    all_masks = mask_to_edge(mask_img, 50)

    labelme = Labelme(h, w, ori_image_path)
    for mask in all_masks:
        labelme.add_shape("polygon", "loose", mask)
    labelme.export()

    print(len(all_masks))
