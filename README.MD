# Mask image to Labelme
### input image
1. Src  
![src](images/src.png)
2. Corresponding mask image  
![mask](images/src_mask.png)

### Labelme annotation UI
1. result (filter out small area)
![result](images/labelme_anno.png)